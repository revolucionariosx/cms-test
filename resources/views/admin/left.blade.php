<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image" style="height:50px">
        {{-- <img src="adminTemplate/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" /> --}}
      </div>
      <div class="pull-left info">
        <p>Alexander Pierce</p>

        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MENU PRINCIPAL</li>
      <li class="" ui-sref-active="active"  >
        <a ui-sref="dashboard">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      <li class="treeview" ui-sref-active="active"  >
        <a href="#">
          <i class="fa fa-file-o"></i> <span>Paginas</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li ng-class="{active: vm.isActive('/paginas')}"><a ui-sref="paginas"><i class="fa fa-circle-o"></i> Listado de Paginas</a></li>
          <li ng-class="{active: vm.isActive('/pagina-nueva')}"><a ui-sref="pagina-nueva"><i class="fa fa-circle-o"></i> Crear Pagina</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-camera-retro"></i> <span>Media</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="../../index.html"><i class="fa fa-circle-o"></i> Listado de Media</a></li>
          <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Crear Media</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-users"></i> <span>Usuarios</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="../../index.html"><i class="fa fa-circle-o"></i> Listado de Usuarios</a></li>
          <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Crear Usuario</a></li>
        </ul>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
