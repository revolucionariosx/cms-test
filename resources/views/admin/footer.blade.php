<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 2.0
  </div>
  <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
</footer>

</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="adminTemplate/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="adminTemplate/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="adminTemplate/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<!-- FastClick -->
<script src='adminTemplate/plugins/fastclick/fastclick.min.js'></script>
<!-- AdminLTE App -->
<script src="adminTemplate/dist/js/app.min.js" type="text/javascript"></script>
<!-- Angularjs -->
<script src="bower_components/angular/angular.js" type="text/javascript"></script>
<script src="bower_components/angular-ui-router/release/angular-ui-router.min.js" type="text/javascript"></script>
<script src="bower_components/angular-loading-bar/build/loading-bar.min.js" type="text/javascript"></script>
<link rel='stylesheet' href="bower_components/angular-loading-bar/build/loading-bar.min.css" type='text/css' media='all' />
<script src="scripts/app.js" type="text/javascript"></script>
<script src="scripts/controllers/panelController.js" type="text/javascript"></script>

<!-- Demo -->
<script src="adminTemplate/dist/js/demo.js" type="text/javascript"></script>
</body>
</html>
