(function() {
	'use strict';

	angular.
		module('adminPanel', [
			'ui.router',
			'angular-loading-bar'

		]) //aqui configuramos nuestras rutas para el panel administrativo
		.config(function($stateProvider, $urlRouterProvider) {
		  //
		  // Cualquier url erronea redirige a  /dashboard
		  $urlRouterProvider.otherwise("/dashboard");
		  //
		  // Seteamos los estados
		  $stateProvider
		    .state('dashboard', {
		      url: "/dashboard",
		      templateUrl: "scripts/partials/dashboard/index.html"
		    })
		    .state('paginas', {
		      url: "/paginas",
		      templateUrl: "scripts/partials/paginas/paginas.html"
		    })
		    .state('pagina-nueva', {
		      url: "/pagina-nueva",
		      templateUrl: "scripts/partials/paginas/nuevaPagina.html",
		    });
		});
})();
