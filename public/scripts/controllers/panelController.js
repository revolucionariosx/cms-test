(function() {
	'use strict';

	angular.
		module('adminPanel').
			controller('panelController', panelController);

	function panelController($location, $scope) {

		var vm = this;
		//Con esta funcion vemos que elemento del submenu es el activo
		vm.isActive = function (viewLocation) { console.log(viewLocation + '===' + $location.url());
			return viewLocation === $location.url();
		}
	}//fin de adminCtrl
})();
